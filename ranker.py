# This program is based off of Fio4ri's "Favorite Pokemon" webpage. This webpage can be found on github at
# https://fio4ri.github.io/FavoritePokemon/

# Unlike Fio4ri's webpage, this program let's you narrow down ANY list! Simply create a .txt file with one item on each
# line, and this program will help you rank each individual item from that list! 
# This program is mathematically accurate - the matchups are the same each time and will always produce the same rankings.

import math
import random
import sys
import tkinter
import os
import itertools
from tkinter.filedialog import askopenfilename

do_debug = 0


def s_input(string):
    if do_debug:
        print(string, end='')
        char = random.choice('abcdefghijklmnopqrstuvwxyz1234567890')
        print(char)

        return char

    x = input(string)

    return x


def setup_list():
    # Extract a list of elements from a .txt file, and format it to make everything work properly
    the_list = []
    list_name = ''

    tkinter.Tk().withdraw()
    filename = askopenfilename(initialdir="Lists" if os.path.exists("Lists") else "")

    with open(filename) as f:
        for line in f:
            the_list.append(line.strip("\n1"))

    new_list = []

    for item in the_list:
        if not ((item.startswith("##") and item.endswith("##")) or not item or item.isspace()):
            new_list.append(item)

    the_list = new_list

    if len(the_list) != len(set(the_list)):
        print("Duplicates found in the list - dupilcates have been removed.")
        the_list = list(set(the_list))

    if len(the_list) < 2:
        s_input("Not enough list items - need at least two! Press enter/return to close the program. ")
        sys.exit()

    if len(the_list) <= 10:
        print(f"Not enough list items for a top ten. Top {math.floor(len(the_list)/2)} will be calculated instead.")
        top_num = math.floor(len(the_list)/2)

    else:
        top_num = 10

    while not list_name:
        list_name = s_input("What are you trying to find your top ten for? (e.g. Songs, Games): ").strip()

    while list_name.lower().startswith("favorite "):
        list_name = list_name[9:]

    while True:
        print("-" * 25)
        print("Choose a ranking algorithm: ")
        print("      [1] 25% Accuracy [Not Yet Available]")
        print("      [2] 50% Accuracy [Not Yet Available]")
        print("      [3] 70% Accuracy [Not Yet Available]")
        print("      [4] 100% Accuracy")
        print("      [5] Explain what these mean")

        while True:
            chosen = s_input("Input [#]: ")

            if chosen == '1':
                break

            if chosen == '2':
                break

            if chosen == '3':
                break

            if chosen == '4':
                matchup_list, rankings = generate_matchups(1.00, the_list)
                start_ranking(matchup_list, rankings, top_num, list_name)

            if chosen == '5':
                print("-" * 25)
                print("""\
The ranking algorithm has you go through every possible matchup of two items on
your list, and choose which one you think is best. At 25% Accuracy, only 1/4 of
these matchups (spread evenly so all items have an equal number of matchups) 
will take place, up to ALL matchups at 100%. In the event of small lists, all
items will always have at least two matchups on any setting.""")
                s_input("Press enter/return")

                break


def generate_matchups(accuracy, the_list):
    matchup_list = [x for x in itertools.combinations(the_list, 2)]
    rankings = {x: [0, 0] for x in the_list}
    random.shuffle(matchup_list)
    return matchup_list, rankings


def start_ranking(matchup_list, rankings, top_num, list_name):
    for num, matchup in enumerate(matchup_list):
        padding_1 = max(len(matchup[0]), len(matchup[1])) - len(matchup[0]) + 1
        padding_2 = max(len(matchup[0]), len(matchup[1])) - len(matchup[1]) + 1
        percentage = round(num/len(matchup_list)*100, 1)
        vote_ratio1 = f"{rankings[matchup[0]][0]}/{rankings[matchup[0]][1]}"
        vote_ratio2 = f"{rankings[matchup[1]][0]}/{rankings[matchup[1]][1]}"

        print('-' * 25)
        print(f"Progress: {num}/{len(matchup_list)} matchups completed [{percentage}%]")
        print("Choose your FAVORITE of these two options: ")
        print(f"      [1] {matchup[0]}{' '*padding_1}| voted for {vote_ratio1} times")
        print(f"      [2] {matchup[1]}{' '*padding_2}| voted for {vote_ratio2} times")

        while True:
            chosen = s_input('Input [#]: ')

            if chosen == "1":
                rankings[matchup[0]][0] += 1
                rankings[matchup[0]][1] += 1
                rankings[matchup[1]][1] += 1
                break

            elif chosen == "2":
                rankings[matchup[1]][0] += 1
                rankings[matchup[0]][1] += 1
                rankings[matchup[1]][1] += 1
                break

    results = [x[0] for x in sorted(rankings.items(), key=lambda kv: kv[1], reverse=True)]
    show_results(results, top_num, list_name)


def show_results(results, top_num, list_name):
    while True:
        print('-'*25)
        print(f"The top {top_num} have been decided! And you can view the full rankings too!")
        print(f"      [T]op {top_num}")
        print(f"      [F]ull list (Probably not smart for big lists...)")
        print(f"      [I]'m done looking at my results")

        while True:
            chosen2 = s_input(f'Input [L]etter: ').lower()

            if chosen2.startswith("t"):
                chosen_list = results[:top_num]
                chosen_word = f"Your top {top_num} FAVORITE"

            elif chosen2.startswith("f"):
                chosen_list = results
                chosen_word = "Your tier list for"

            elif chosen2.startswith("i"):
                export_results(results, list_name)
                sys.exit()

            else:
                continue

            print('-'*25)
            print(f"{chosen_word} {list_name}: ")

            for num, item in enumerate(chosen_list):
                print(f"{num + 1}. {item}")

            s_input("\nPress enter/return ")
            print('-'*25)

            break


def export_results(full_list, list_name):
    print("-"*25)
    while True:
        y_n = s_input("Export results to file? | Yes or No: ").lower()

        if y_n.startswith("y"):
            x = 1
            while True:
                if not os.path.isdir("Output"):
                    os.makedirs("Output")

                if os.path.exists(f"Output/{list_name.lower()}_{x}.txt"):
                    x += 1

                else:
                    with open(f"Output/{list_name.lower()}_{x}.txt", mode='w+') as f:
                        f.write(f"Your tier list for {list_name}:\n")
                        f.writelines(f"{num + 1}. {x}\n" for num, x in enumerate(full_list))

                    print(f"Results have been written to `Output/{list_name.lower()}_{x}.txt`")
                    s_input("Press enter/return ")

                    return

        if y_n.startswith("n"):
            return


setup_list()
